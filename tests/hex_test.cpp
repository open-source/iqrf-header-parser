/**
 * Copyright 2021-2025 IQRF Tech s.r.o.
 * Copyright 2021-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include "iqrf-header-parser/iqrf_header_parser.h"

namespace ihp = iqrf_header_parser;

/// iqrf header parser hex test namespace
namespace hex_test {
	/// hex plugin test case
	class HexTest : public ::testing::Test {
	protected:
		std::string record, error;
		std::string noException = "No exception thrown.";
		std::string wrongException = "Exception other than std::invalid_argument thrown.";
		uint8_t os, mcu, tr;
		ihp::device::ModuleInfo moduleInfo;

		void SetUp() {
			moduleInfo.osMajor = 4;
			moduleInfo.osMinor = 6;
			moduleInfo.mcuType = ihp::device::MCU::PIC16LF18877;
			moduleInfo.trSeries = ihp::device::TRG::TR_72G;
		}
	};

	TEST_F(HexTest, ValidateRecord_Valid_Data) {
		record = ":02020000FFFFFE";
		EXPECT_NO_THROW(ihp::hex::validateRecord(record));
	}

	TEST_F(HexTest, ValidateRecord_Not_Hex_Record) {
		record = "02020000FFFFFE";
		error = "Intel HEX record should start with a colon character.";
		try {
			ihp::hex::validateRecord(record);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateRecord_Too_Short) {
		record = ":00020000F";
		error = "Intel HEX record length should be at least 11 characters long.";
		try {
			ihp::hex::validateRecord(record);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateRecord_Too_Long) {
		record = ":" + std::string(600, 'F');
		error = "Intel HEX record length should be at most 521 characters long.";
		try {
			ihp::hex::validateRecord(record);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateRecord_Even_Length) {
		record = ":02020000FFFFF";
		error = "Intel HEX record should contain odd number of characters.";
		try {
			ihp::hex::validateRecord(record);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateRecord_Invalid_Characters) {
		record = ":04020000FFFFFFFXFE";
		error = "Intel HEX record should contain only hexadecimal characters.";
		try {
			ihp::hex::validateRecord(record);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateRecord_Invalid_Checksum) {
		record = ":02020000FFFFFF";
		error = "Incorrect Intel HEX record checksum.";
		try {
			ihp::hex::validateRecord(record);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateRecord_Invalid_End_Of_File_Record) {
		record = ":0100000100FE";
		error = "Invalid Intel HEX end-of-file record.";
		try {
			ihp::hex::validateRecord(record);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateRecord_Unknown_Unsupported_Record_Type) {
		record = ":02020005FFFFF9";
		error = "Unknown or unsupported Intel HEX record type.";
		try {
			ihp::hex::validateRecord(record);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateRecord_Byte_Count_Mismatch) {
		record = ":04020000FFFFFC";
		error = "Byte count and detected data byte count mismatch: expected " + std::to_string(4) + " detected " + std::to_string(2);
		try {
			ihp::hex::validateRecord(record);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateChecksum_Wrong_Checksum_Value) {
		record = ":02020000FFFFFF";
		EXPECT_FALSE(ihp::hex::validRecordChecksum(record));
	}

	TEST_F(HexTest, ParseCompatibilityHeader_4B_OK) {
		record = ":047000004600050041";
		EXPECT_NO_THROW(ihp::hex::parseCompatibilityHeader(record, os, mcu, tr));
		EXPECT_EQ(46, os);
		EXPECT_EQ(5, mcu);
		EXPECT_EQ(1, tr);
	}

	TEST_F(HexTest, ParseCompatibilityHeader_6B_OK) {
		record = ":06700000460005008200BD";
		EXPECT_NO_THROW(ihp::hex::parseCompatibilityHeader(record, os, mcu, tr));
		EXPECT_EQ(46, os);
		EXPECT_EQ(5, mcu);
		EXPECT_EQ(2, tr);
	}

	TEST_F(HexTest, ParseCompatibilityHeader_Invalid) {
		record = ":0570000046000500000041";
		error = "Invalid compatibility header.";
		try {
			ihp::hex::parseCompatibilityHeader(record, os, mcu, tr);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateDeviceCompatibility_4B_Compatible) {
		record = ":047000004500040043";
		moduleInfo.osMajor = 4;
		moduleInfo.osMinor = 5;
		moduleInfo.mcuType = ihp::device::MCU::PIC16LF1938;
		moduleInfo.trSeries = ihp::device::TRD::TR_72D;
		EXPECT_NO_THROW(ihp::hex::validateDeviceCompatibility(record, moduleInfo));
	}

	TEST_F(HexTest, ValidateDeviceCompatibility_6B_Compatible) {
		record = ":06700000460005008200BD";
		EXPECT_NO_THROW(ihp::hex::validateDeviceCompatibility(record, moduleInfo));
	}

	TEST_F(HexTest, ValidateDeviceCompatibility_Invalid_Byte_Count) {
		record = ":0570000046000500000041";
		error = "Identification header record should have 4 or 6 data bytes.";
		try {
			ihp::hex::validateDeviceCompatibility(record, moduleInfo);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateDeviceCompatibility_6B_OS_Mismatch) {
		record = ":06700000450005008200BE";
		error = "Selected HEX is for OS 45, but the device OS is " + std::to_string(moduleInfo.osMajor * 10 + moduleInfo.osMinor);
		try {
			ihp::hex::validateDeviceCompatibility(record, moduleInfo);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateDeviceCompatibility_6B_MCU_Mismatch) {
		record = ":06700000460004008200BE";
		error = "Selected HEX is for MCU 4, but the device MCU is " + std::to_string(moduleInfo.mcuType);
		try {
			ihp::hex::validateDeviceCompatibility(record, moduleInfo);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateDeviceCompatibility_6B_Invalid_MCU_TR_Combination) {
		record = ":06700000460004008200BE";
		error = "Identification header has invalid combination of MCU (4) and TR family (2).";
		moduleInfo.mcuType = ihp::device::MCU::PIC16LF1938;
		moduleInfo.trSeries = ihp::device::TRD::TR_72D;
		try {
			ihp::hex::validateDeviceCompatibility(record, moduleInfo);
		FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateDeviceCompatibility_6B_Unknown_Device) {
		record = ":06700000460005008200BD";
		error = "Unable to identify device family.";
		moduleInfo.trSeries = 1;
		try {
			ihp::hex::validateDeviceCompatibility(record, moduleInfo);
			FAIL() << noException;
		} catch (const std::domain_error &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(HexTest, ValidateDeviceCompatibility_6B_TR_Mismatch) {
		record = ":06700000460005008300BC";
		error = "Selected HEX is for TR family " + std::to_string(ihp::TrFamily::TR_8xG) + ", but the device TR family is " + std::to_string(ihp::device::TrFamily::TR_7xG);
		try {
			ihp::hex::validateDeviceCompatibility(record, moduleInfo);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}
}
