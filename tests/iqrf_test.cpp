/**
 * Copyright 2021-2025 IQRF Tech s.r.o.
 * Copyright 2021-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include "iqrf-header-parser/iqrf_header_parser.h"

namespace ihp = iqrf_header_parser;

/// iqrf header parser iqrf test namespace
namespace iqrf_test {
	/// iqrf plugin test class
	class IqrfTest : public ::testing::Test {
	protected:
		ihp::ModuleInfo moduleInfo = ihp::ModuleInfo();
		std::string str, error, expected;
		std::vector<std::string> v;
		uint8_t updateTime, mcu, tr;

		void SetUp() {
			str = error = expected = "";
			updateTime = 0;
			moduleInfo.osMajor = 4;
			moduleInfo.osMinor = 4;
			moduleInfo.osBuild = 2261;
			moduleInfo.mcuType = ihp::device::MCU::PIC16LF1938;
			moduleInfo.trSeries = ihp::device::TRD::TR_72D;
			mcu = 4;
			tr = 1;
		}
	};

	TEST_F(IqrfTest, ValidateMcuHeader_OK) {
		str = "#$41";
		EXPECT_NO_THROW(ihp::iqrf::validateMcuHeader(str));
		moduleInfo.mcuType = ihp::device::MCU::PIC16LF18877;
		moduleInfo.trSeries = ihp::device::TRG::TR_72G;
		str = "#$52";
		EXPECT_NO_THROW(ihp::iqrf::validateMcuHeader(str));
	}

	TEST_F(IqrfTest, ValidateMcuHeader_Invalid) {
		str = "invalid header";
		EXPECT_THROW(ihp::iqrf::validateMcuHeader(str), std::invalid_argument);
		str = "#$ 41";
		EXPECT_THROW(ihp::iqrf::validateMcuHeader(str), std::invalid_argument);
		str = "#$11";
		EXPECT_THROW(ihp::iqrf::validateMcuHeader(str), std::domain_error);
		str = "#$47";
		EXPECT_THROW(ihp::iqrf::validateMcuHeader(str), std::domain_error);
	}

	TEST_F(IqrfTest, ParseMcuHeader_OK) {
		str = "#$41";
		EXPECT_NO_THROW(ihp::iqrf::parseMcuHeader(str, mcu, tr));
		EXPECT_EQ(mcu, 4);
		EXPECT_EQ(tr, 1);
		str = "#$52";
		EXPECT_NO_THROW(ihp::iqrf::parseMcuHeader(str, mcu, tr));
		EXPECT_EQ(mcu, 5);
		EXPECT_EQ(tr, 2);
	}

	TEST_F(IqrfTest, MCU_Header_Valid) {
		str = "#$41";
		EXPECT_EQ(ihp::iqrf::parseMcuHeader(str, moduleInfo.mcuType, moduleInfo.trSeries, error), ihp::Error::NO_ERROR);
		EXPECT_EQ(error, "");
	}

	TEST_F(IqrfTest, MCU_Header_Invalid) {
		str = "invalid header";
		expected = "Invalid MCU/TR header format (1). Header: " + str;
		EXPECT_EQ(ihp::iqrf::parseMcuHeader(str, moduleInfo.mcuType, moduleInfo.trSeries, error), ihp::Error::INVALID_FILE_CONTENT);
		EXPECT_EQ(error, expected);
		str = "#$ 41";
		expected = "Invalid MCU/TR header format (1). Header: " + str;
		EXPECT_EQ(ihp::iqrf::parseMcuHeader(str, moduleInfo.mcuType, moduleInfo.trSeries, error), ihp::Error::INVALID_FILE_CONTENT);
		EXPECT_EQ(error, expected);
		str = "#$11";
		expected = "Invalid MCU type in header (1). MCU type: 1";
		EXPECT_EQ(ihp::iqrf::parseMcuHeader(str, moduleInfo.mcuType, moduleInfo.trSeries, error), ihp::Error::INVALID_FILE_CONTENT);
		EXPECT_EQ(error, expected);
		str = "#$47";
		expected = "Invalid TR series in header (1). TR series: 7";
		EXPECT_EQ(ihp::iqrf::parseMcuHeader(str, moduleInfo.mcuType, moduleInfo.trSeries, error), ihp::Error::INVALID_FILE_CONTENT);
		EXPECT_EQ(error, expected);
	}

	TEST_F(IqrfTest, MCU_Header_Device_Incompatible) {
		str = "#$51";
		expected = "Selected IQRF plugin is not compatible with the module MCU type. Module MCU type: 4, header MCU type: 5";
		EXPECT_EQ(ihp::iqrf::parseMcuHeader(str, moduleInfo.mcuType, moduleInfo.trSeries, error), ihp::Error::DEVICE_INCOMPATIBLE_FILE);
		EXPECT_EQ(error, expected);
		moduleInfo.trSeries = ihp::TR_72D;
		str = "#$40";
		expected = "Selected IQRF plugin is for TR-5xD series devices, but the device is not from TR-5xD series.";
		EXPECT_EQ(ihp::iqrf::parseMcuHeader(str, moduleInfo.mcuType, moduleInfo.trSeries, error), ihp::Error::DEVICE_INCOMPATIBLE_FILE);
		EXPECT_EQ(error, expected);
		moduleInfo.trSeries = ihp::TR_52D;
		str = "#$41";
		expected = "Selected IQRF plugin is for TR-7xD series devices, but the device is not from TR-7xD series.";
		EXPECT_EQ(ihp::iqrf::parseMcuHeader(str, moduleInfo.mcuType, moduleInfo.trSeries, error), ihp::Error::DEVICE_INCOMPATIBLE_FILE);
		EXPECT_EQ(error, expected);
		moduleInfo.mcuType = ihp::PIC16LF18877;
		str = "#$52";
		expected = "Selected IQRF plugin is for TR-7xG series devices, but the device is not from TR-7xG series.";
		EXPECT_EQ(ihp::iqrf::parseMcuHeader(str, moduleInfo.mcuType, moduleInfo.trSeries, error), ihp::Error::DEVICE_INCOMPATIBLE_FILE);
		EXPECT_EQ(error, expected);
		moduleInfo.trSeries = ihp::TR_72D;
		str = "#$53";
		expected = "Selected IQRF plugin is for TR-8xG series devices, but the device is not from TR-8xG series.";
		EXPECT_EQ(ihp::iqrf::parseMcuHeader(str, moduleInfo.mcuType, moduleInfo.trSeries, error), ihp::Error::DEVICE_INCOMPATIBLE_FILE);
		EXPECT_EQ(error, expected);
	}

	TEST_F(IqrfTest, OS_Header_Valid) {
		str = "#$44";
		EXPECT_EQ(ihp::iqrf::parseOsHeader(str, moduleInfo, error), ihp::Error::NO_ERROR);
		EXPECT_EQ(error, expected);
		str = "#$31;32F9040005;32F905;44";
		EXPECT_EQ(ihp::iqrf::parseOsHeader(str, moduleInfo, error), ihp::Error::NO_ERROR);
		EXPECT_EQ(error, expected);
		str = "#$44C808D708";
		EXPECT_EQ(ihp::iqrf::parseOsHeader(str, moduleInfo, error), ihp::Error::NO_ERROR);
		EXPECT_EQ(error, expected);
	}

	TEST_F(IqrfTest, OS_Header_Invalid) {
		expected = "Invalid OS header format (2). Header: " + str;
		EXPECT_EQ(ihp::iqrf::parseOsHeader(str, moduleInfo, error), ihp::Error::INVALID_FILE_CONTENT);
		EXPECT_EQ(error, expected);
		str = "#$ 44";
		expected = "Invalid OS header format (2). Header: " + str;
		EXPECT_EQ(ihp::iqrf::parseOsHeader(str, moduleInfo, error), ihp::Error::INVALID_FILE_CONTENT);
		EXPECT_EQ(error, expected);
		str = " #$ 44";
		expected = "Invalid OS header format (2). Header: " + str;
		EXPECT_EQ(ihp::iqrf::parseOsHeader(str, moduleInfo, error), ihp::Error::INVALID_FILE_CONTENT);
		EXPECT_EQ(error, expected);
		str = "#$ 444";
		expected = "Invalid OS header format (2). Header: " + str;
		EXPECT_EQ(ihp::iqrf::parseOsHeader(str, moduleInfo, error), ihp::Error::INVALID_FILE_CONTENT);
		EXPECT_EQ(error, expected);
		str = "#$3244";
		expected = "Invalid OS header format (2). Header: " + str;
		EXPECT_EQ(ihp::iqrf::parseOsHeader(str, moduleInfo, error), ihp::Error::INVALID_FILE_CONTENT);
		EXPECT_EQ(error, expected);
		str = "#$32;4408D5F";
		expected = "Invalid OS header format (2). Header: " + str;
		EXPECT_EQ(ihp::iqrf::parseOsHeader(str, moduleInfo, error), ihp::Error::INVALID_FILE_CONTENT);
		EXPECT_EQ(error, expected);
	}

	TEST_F(IqrfTest, OS_Header_Device_Incompatible) {
		str = "#$32";
		expected = "Selected IQRF plugin is not compatible with device OS 44(08D5)";
		EXPECT_EQ(ihp::iqrf::parseOsHeader(str, moduleInfo, error), ihp::Error::DEVICE_INCOMPATIBLE_FILE);
		EXPECT_EQ(error, expected);
		str = "#$43;44D708";
		EXPECT_EQ(ihp::iqrf::parseOsHeader(str, moduleInfo, error), ihp::Error::DEVICE_INCOMPATIBLE_FILE);
		EXPECT_EQ(error, expected);
		str = "#$43D508;44D708";
		EXPECT_EQ(ihp::iqrf::parseOsHeader(str, moduleInfo, error), ihp::Error::DEVICE_INCOMPATIBLE_FILE);
		EXPECT_EQ(error, expected);
	}

	TEST_F(IqrfTest, Plugin_Header_DPA_Valid) {
		str = "#$Plug-in: DPA-Coordinator-SPI-7xD-V414-200403";
		EXPECT_TRUE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Coordinator-UART-7xD-V414-200403";
		EXPECT_TRUE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Node-LP-7xD-V414-200403";
		EXPECT_TRUE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Node-STD-7xD-V414-200403";
		EXPECT_TRUE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Node-STD-SPI-7xD-V414-200403";
		EXPECT_TRUE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Node-STD-UART-7xD-V414-200403";
		EXPECT_TRUE(ihp::iqrf::validPluginHeaderDpa(str));
	}

	TEST_F(IqrfTest, Plugin_Header_DPA_Invalid) {
		str = " #$Plug-in: DPA-Coordinator-SPI-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$ Plug-in: DPA-Coordinator-SPI-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in:  DPA-Coordinator-SPI-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Coordinator-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Coordinator-LP-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Coordinator-STD-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Coordinator-SPI-7xD-V4141-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Coordinator-SPI-7xD-414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-SPI-Coordinator-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-coordinator-SPI-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Coordinator-SPI-7xD-V414-2004";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Coordinator-SPI-UART-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = " #$Plug-in: DPA-Node-SPI-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in:  DPA-Node-SPI-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$ Plug-in: DPA-Node-SPI-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Node-SPI-7xD-V4141-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Node-SPI-7xD-414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-SPI-Node-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-node-SPI-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Node-SPI-7xD-V414-2004";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Node-SPI-UART-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Node-LP-SPI-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
		str = "#$Plug-in: DPA-Node-LP-UART-7xD-V414-200403";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderDpa(str));
	}

	TEST_F(IqrfTest, Plugin_Header_OS_Valid) {
		str = "#$Plug-in: ChangeOS-TR7x-403(08C8)-404(08D5)";
		EXPECT_TRUE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-402(08B8)-403(08C8)-1of2";
		EXPECT_TRUE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-402(08B8)-403(08C8)-2of2";
		EXPECT_TRUE(ihp::iqrf::validPluginHeaderOs(str));
	}

	TEST_F(IqrfTest, Plugin_Header_OS_Invalid) {
		str = " #$Plug-in: ChangeOS-TR7x-403(08C8)-404(08D5)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in:  ChangeOS-TR7x-403(08C8)-404(08D5)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$ Plug-in: ChangeOS-TR7x-403(08C8)-404(08D5)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: changeOS-TR7x-403(08C8)-404(08D5)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR77x-403(08C8)-404(08D5)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7.5x-403(08C8)-404(08D5)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-4031(08C8)-404(08D5)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-40(08C8)-404(08D5)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-403(08C8)-4041(08D5)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-403(08C8)-40(08D5)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-(08C8)403-(08D5)404";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-403(08CX)-404(08D5)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-403(08C8)-404(08DX)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-403(08C)-404(08D5)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-403(08C8A)-404(08D5)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-403(08C8)-404(08D)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-403(08C8)-404(08D5A)";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-403(08C8)-404(08D5)-";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-403(08C8)-404(08D5)-1of";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
		str = "#$Plug-in: ChangeOS-TR7x-402(08B8)-403(08C8)-3of2";
		EXPECT_FALSE(ihp::iqrf::validPluginHeaderOs(str));
	}

	TEST_F(IqrfTest, Update_Header_Valid) {
		uint8_t expected = 5;
		EXPECT_NE(updateTime, expected);
		str = "#$UpdateTime: 5";
		EXPECT_TRUE(ihp::iqrf::parseUpdateHeader(str, updateTime));
		EXPECT_EQ(updateTime, expected);
	}

	TEST_F(IqrfTest, Update_Header_Invalid) {
		uint8_t expected = 5;
		EXPECT_NE(updateTime, expected);
		str = "#$ UpdateTime: 5";
		EXPECT_FALSE(ihp::iqrf::parseUpdateHeader(str, updateTime));
		EXPECT_NE(updateTime, expected);
		str = "#$ UpdateTime: 5s";
		EXPECT_FALSE(ihp::iqrf::parseUpdateHeader(str, updateTime));
		EXPECT_NE(updateTime, expected);
		str = "#$UpdateTime:  5";
		EXPECT_FALSE(ihp::iqrf::parseUpdateHeader(str, updateTime));
		EXPECT_NE(updateTime, expected);
		str = "#$UpdateTime: 5.5";
		EXPECT_FALSE(ihp::iqrf::parseUpdateHeader(str, updateTime));
		EXPECT_NE(updateTime, expected);
		str = "#$UpdateTime: -1";
		EXPECT_FALSE(ihp::iqrf::parseUpdateHeader(str, updateTime));
		EXPECT_NE(updateTime, expected);
	}

	TEST_F(IqrfTest, Data_Valid) {
		str = "ffffffffffffffffffffffffffffffffffffffff";
		EXPECT_TRUE(ihp::iqrf::validDataLine(str, error));
		str = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
		EXPECT_TRUE(ihp::iqrf::validDataLine(str, error));
	}

	TEST_F(IqrfTest, Data_Invalid) {
		str = "fffffffffffffffffffffffffffffffffffffff";
		expected = "IQRF plugin data line should be 40 characters long.";
		EXPECT_FALSE(ihp::iqrf::validDataLine(str, error));
		EXPECT_EQ(error, expected);
		str = "xfffffffffffffffffffffffffffffffffffffff";
		expected = "IQRF plugin data line contains non-hexadecimal characters.";
		EXPECT_FALSE(ihp::iqrf::validDataLine(str, error));
		EXPECT_EQ(error, expected);
		str = "fffffffffffffffffffffffffffffffffffffffX";
		EXPECT_FALSE(ihp::iqrf::validDataLine(str, error));
		EXPECT_EQ(error, expected);
	}

	TEST_F(IqrfTest, Separator_Valid) {
		EXPECT_TRUE(ihp::iqrf::isSeparator("# *"));
		EXPECT_TRUE(ihp::iqrf::isSeparator("# ***********************"));
	}

	TEST_F(IqrfTest, Separator_Invalid) {
		EXPECT_FALSE(ihp::iqrf::isSeparator(" # *"));
		EXPECT_FALSE(ihp::iqrf::isSeparator("#  *"));
		EXPECT_FALSE(ihp::iqrf::isSeparator("# * *"));
		EXPECT_FALSE(ihp::iqrf::isSeparator("## *"));
	}
}
