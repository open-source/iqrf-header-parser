/**
 * Copyright 2021-2025 IQRF Tech s.r.o.
 * Copyright 2021-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include "iqrf-header-parser/iqrf_header_parser.h"

namespace ihp = iqrf_header_parser;

/// iqrf header parser utils test namespace
namespace utils_test {
	/// utils test class
	class UtilsTest : public ::testing::Test {
	protected:
		std::string str, delim, error;
		std::string noException = "No exception thrown.";
		std::string wrongException = "Exception other than std::invalid_argument thrown.";
		std::vector<std::string> v;
		char c;
		uint8_t byte;

		void SetUp() {
			delim = ";";
		}
	};

	TEST_F(UtilsTest, Split_Empty_String) {
		EXPECT_EQ(v, ihp::utils::split(str, delim));
	}

	TEST_F(UtilsTest, Split_NoMatch) {
		str = "TEST:STRING:TO:SPLIT";
		v.push_back(str);
		EXPECT_EQ(v, ihp::utils::split(str, delim));
	}

	TEST_F(UtilsTest, Split_Match) {
		str = "TEST;STRING;TO;SPLIT";
		v = {"TEST", "STRING", "TO", "SPLIT"};
		EXPECT_EQ(v, ihp::utils::split(str, delim));
	}

	TEST_F(UtilsTest, Split_Match_Consecutive_Occurences) {
		str = "TEST;;STRING;TO;SPLIT";
		v = {"TEST", "", "STRING", "TO", "SPLIT"};
		EXPECT_EQ(v, ihp::utils::split(str, delim));
	}

	TEST_F(UtilsTest, Split_Only_Delimiters) {
		str = ";;;;";
		v = {"", "", "", "", ""};
		EXPECT_EQ(v, ihp::utils::split(str, delim));
	}

	TEST_F(UtilsTest, Split_Only_Delimeter_Mix) {
		str = ";;;;;";
		delim = ";;";
		v = {"", "", ";"};
		EXPECT_EQ(v, ihp::utils::split(str, delim));
	}

	TEST_F(UtilsTest, CharToUint8) {
		c = '5';
		EXPECT_EQ(5, ihp::utils::charToUint8(c));
		c = '3';
		EXPECT_EQ(3, ihp::utils::charToUint8(c));
	}

	TEST_F(UtilsTest, HexStringToByte_Valid) {
		str = "FF";
		byte = 255;
		EXPECT_EQ(byte, ihp::utils::hexStringToByte(str, 0));
		str = "FF0F";
		byte = 15;
		EXPECT_EQ(byte, ihp::utils::hexStringToByte(str, 2));
	}

	TEST_F(UtilsTest, HexStringToByte_Empty_String) {
		str = "";
		error = "Empty string.";
		try {
			ihp::utils::hexStringToByte(str, 0);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(UtilsTest, HexStringToByte_Invalid_Position) {
		str = "FF",
		error = "Position is further than the length of the string.";
		try {
			ihp::utils::hexStringToByte(str, 2);
			FAIL() << noException;
		} catch (const std::range_error &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}

	TEST_F(UtilsTest, HexStringToByte_Invalid_Hex_String) {
		str = "XY";
		error = "Invalid hexadecimal string: " + str;
		try {
			ihp::utils::hexStringToByte(str, 0);
			FAIL() << noException;
		} catch (const std::invalid_argument &e) {
			EXPECT_EQ(error, e.what());
		} catch (...) {
			FAIL() << wrongException;
		}
	}
}
