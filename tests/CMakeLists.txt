# Copyright 2021-2025 IQRF Tech s.r.o.
# Copyright 2021-2025 MICRORISC s.r.o.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

project(iqrf-header-parser-test)

enable_testing()
find_package(GTest QUIET)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../include)

if(GTest_FOUND)
	include_directories(${GTEST_INCLUDE_DIRS})
else()
	add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../external/googletest googletest EXCLUDE_FROM_ALL)
endif()

file(GLOB TEST_FILES "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

add_executable(${PROJECT_NAME} ${TEST_FILES})
target_link_libraries(${PROJECT_NAME} gtest gtest_main)
if (IHP_BUILD_STATIC)
	target_link_libraries(${PROJECT_NAME} iqrf-header-parser_static)
else()
	target_link_libraries(${PROJECT_NAME} iqrf-header-parser)
endif()

include(GoogleTest)
gtest_discover_tests(${PROJECT_NAME})
