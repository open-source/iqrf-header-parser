# Copyright 2021-2025 IQRF Tech s.r.o.
# Copyright 2021-2025 MICRORISC s.r.o.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.PHONY: all build coverage coverage-html coverage-xml

build:
	cmake -Bbuild -H. -DCMAKE_BUILD_TYPE=Debug
	cmake --build build

build-clean: clean build

clean:
	rm -rf build

test:
	cmake -Bbuild -H. -DCMAKE_BUILD_TYPE=Debug -DIHP_BUILD_TESTS=True
	cmake --build build
	cd build/tests/ && ctest -V --output-junit ../../ctest.xml

coverage:
	cmake -Bbuild -H. -DCMAKE_BUILD_TYPE=Debug -DCODE_COVERAGE=1 -DIHP_BUILD_TESTS=True
	cmake --build build
	cd build/tests/ && ctest -V --output-junit ../../ctest.xml

coverage-html: coverage
	mkdir -p coverage
	cd build/tests/ && gcovr --html-details --exclude-unreachable-branches --print-summary -o ../../coverage/index.html --filter ../../src/ --root ../../

coverage-xml: coverage
	cd build/tests/ && gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o ../../coverage.xml --filter ../../src/ --root ../../

deb-package:
	debuild -b -uc -tc -us
