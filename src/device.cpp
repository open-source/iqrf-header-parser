/**
 * Copyright 2021-2025 IQRF Tech s.r.o.
 * Copyright 2021-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iqrf-header-parser/device.h"

/// iqrf header parser namespace
namespace iqrf_header_parser {
	/// device namespace
	namespace device {
		bool validMcuType(const uint8_t &mcuType) {
			return (mcuType == PIC16LF1938 || mcuType == PIC16LF18877);
		}

		TrFamily getTrFamily(const uint8_t &mcu, const uint8_t &tr) {
			if (mcu == PIC16LF1938) {
				switch (tr) {
					case TR_52D:
					case TR_58D_RJ:
					case TR_53D:
					case TR_54D:
					case TR_55D:
					case TR_56D:
						return TrFamily::TR_5xD;
					case TR_72D:
					case TR_78D:
					case TR_76D:
					case TR_75D:
					case TR_77D:
						return TrFamily::TR_7xD;
					default:
						return TrFamily::UNKNOWN_FAMILY;
				}
			} else if (mcu == PIC16LF18877) {
				switch (tr) {
					case TR_72G:
					case TR_76G:
					case TR_75G:
						return TrFamily::TR_7xG;
					case TR_82G:
					case TR_85G:
					case TR_86G:
						return TrFamily::TR_8xG;
					default:
						return TrFamily::UNKNOWN_FAMILY;
				}
			} else {
				return TrFamily::UNKNOWN_FAMILY;
			}
		}

		bool validTrFamily(const uint8_t &trSeries) {
			switch (trSeries) {
				case TR_5xD:
				case TR_7xD:
				case TR_7xG:
				case TR_8xG:
					return true;
				default:
					return false;
			}
		}

		bool validTr5xD(const uint8_t &tr) {
			switch (tr) {
				case TR_52D:
				case TR_58D_RJ:
				case TR_53D:
				case TR_54D:
				case TR_55D:
				case TR_56D:
					return true;
				default:
					return false;
			}
		}

		bool validTr7xD(const uint8_t &tr) {
			switch (tr) {
				case TR_72D:
				case TR_78D:
				case TR_76D:
				case TR_77D:
				case TR_75D:
					return true;
				default:
					return false;
			}
		}

		bool validTr7xG(const uint8_t &tr) {
			switch (tr) {
				case TR_72G:
				case TR_76G:
				case TR_75G:
					return true;
				default:
					return false;
			}
		}

		bool validTr8xG(const uint8_t &tr) {
			switch (tr) {
				case TR_82G:
				case TR_85G:
				case TR_86G:
					return true;
				default:
					return false;
			}
		}

		bool validMcuTrCombination(const uint8_t &mcu, const uint8_t &tr) {
			if (mcu == PIC16LF1938) {
				return (tr == TR_5xD || tr == TR_7xD);
			} else if (mcu == PIC16LF18877) {
				return (tr == TR_7xG || tr == TR_8xG);
			} else {
				return false;
			}
		}
	}
}
