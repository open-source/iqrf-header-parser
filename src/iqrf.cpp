/**
 * Copyright 2021-2025 IQRF Tech s.r.o.
 * Copyright 2021-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iqrf-header-parser/iqrf.h"

/// iqrf header parser namespace
namespace iqrf_header_parser {
	/// iqrf plugin namespace
    namespace iqrf {
		void validateMcuHeader(const std::string &line) {
			if (!std::regex_match(line, std::regex(MCU_HEADER_PATTERN, std::regex_constants::icase))) {
				throw std::invalid_argument("Invalid MCU/TR header format (1). Header: " + line);
			}
			uint8_t mcu = utils::charToUint8(line[2]);
			if (!validMcuType(mcu)) {
				throw std::domain_error("Invalid MCU type in header (1). MCU type: " + std::to_string(mcu));
			}
			uint8_t tr = utils::charToUint8(line[3]);
			if (!validTrFamily(tr)) {
				throw std::domain_error("Invalid TR family in header (1). TR family: " + std::to_string(tr));
			}
			if (!validMcuTrCombination(mcu, tr)) {
				throw std::domain_error("Invalid MCU type and TR family combination in header (1).");
			}
		}

		void parseMcuHeader(const std::string &line, uint8_t &mcu, uint8_t &tr) {
			validateMcuHeader(line);
			mcu = utils::charToUint8(line[2]);
			tr = utils::charToUint8(line[3]);
		}

		void validateMcuCompatibility(const std::string &line, const ModuleInfo &moduleInfo) {
			uint8_t mcu = utils::charToUint8(line[2]);
			uint8_t tr = utils::charToUint8(line[3]);
			if (mcu != moduleInfo.mcuType) {
				std::stringstream ss;
				ss << "Selected IQRF plugin is not compatible with the module MCU type. Module MCU type: "
					<< std::to_string(moduleInfo.mcuType) + ", header MCU type: " << std::to_string(mcu);
				throw std::invalid_argument(ss.str());
			}
			if (!validTrFamily(tr)) {
				throw std::invalid_argument("Invalid TR series in header (1). TR series: " + std::to_string(tr));
			}
			if (tr == TR_5xD && !validTr5xD(moduleInfo.trSeries)) {
				throw std::invalid_argument("Selected IQRF plugin is for TR-5xD series devices, but the device is not from TR-5xD series.");
			}
			if (tr == TR_7xD && !validTr7xD(moduleInfo.trSeries)) {
				throw std::invalid_argument("Selected IQRF plugin is for TR-7xD series devices, but the device is not from TR-7xD series.");
			}
			if (tr == TR_7xG && !validTr7xG(moduleInfo.trSeries)) {
				throw std::invalid_argument("Selected IQRF plugin is for TR-7xG series devices, but the device is not from TR-7xG series.");
			}
			if (tr == TR_8xG && !validTr8xG(moduleInfo.trSeries)) {
				throw std::invalid_argument("Selected IQRF plugin is for TR-8xG series devices, but the device is not from TR-8xG series.");
			}
		}

		Error parseMcuHeader(const std::string &line, const uint8_t &mcu, const uint8_t &tr, std::string &errorMsg) {
			if (!std::regex_match(line, std::regex(MCU_HEADER_PATTERN, std::regex_constants::icase))) {
				errorMsg = "Invalid MCU/TR header format (1). Header: " + line;
				return INVALID_FILE_CONTENT;
			}
			uint8_t hdrMcu = utils::charToUint8(line[2]);
			if (!validMcuType(hdrMcu)) {
				errorMsg = "Invalid MCU type in header (1). MCU type: " + std::to_string(hdrMcu);
				return INVALID_FILE_CONTENT;
			}
			if (mcu != hdrMcu) {
				errorMsg = "Selected IQRF plugin is not compatible with the module MCU type. Module MCU type: "
					+ std::to_string(mcu) + ", header MCU type: " + std::to_string(hdrMcu);
				return DEVICE_INCOMPATIBLE_FILE;
			}
			uint8_t hdrTr = utils::charToUint8(line[3]);
			if (!validTrFamily(hdrTr)) {
				errorMsg = "Invalid TR series in header (1). TR series: " + std::to_string(hdrTr);
				return INVALID_FILE_CONTENT;
			}
			if (hdrTr == TR_5xD && !validTr5xD(tr)) {
				errorMsg = "Selected IQRF plugin is for TR-5xD series devices, but the device is not from TR-5xD series.";
				return DEVICE_INCOMPATIBLE_FILE;
			}
			if (hdrTr == TR_7xD && !validTr7xD(tr)) {
				errorMsg = "Selected IQRF plugin is for TR-7xD series devices, but the device is not from TR-7xD series.";
				return DEVICE_INCOMPATIBLE_FILE;
			}
			if (hdrTr == TR_7xG && !validTr7xG(tr)) {
				errorMsg = "Selected IQRF plugin is for TR-7xG series devices, but the device is not from TR-7xG series.";
				return DEVICE_INCOMPATIBLE_FILE;
			}
			if (hdrTr == TR_8xG && !validTr8xG(tr)) {
				errorMsg = "Selected IQRF plugin is for TR-8xG series devices, but the device is not from TR-8xG series.";
				return DEVICE_INCOMPATIBLE_FILE;
			}
			return NO_ERROR;
		}

		void validateOsHeader(const std::string &line) {
			if (!std::regex_match(line, std::regex(OS_HEADER_PATTERN, std::regex_constants::icase))) {
				throw std::invalid_argument("Invalid OS header format (2). Header: " + line);
			}
		}

		void parseOsHeader(const std::string &line, std::vector<std::string> &os) {
			validateOsHeader(line);
			os = utils::split(line, ";");
		}

		bool osCompatible(const std::vector<std::string> &osTokens, const ModuleInfo &moduleInfo) {
			char aux[5];
			sprintf(aux, "%X%X", moduleInfo.osMajor, moduleInfo.osMinor);
			std::string deviceOsVersion(aux);
			sprintf(aux, "%04X", moduleInfo.osBuild);
			std::string deviceOsBuild(aux);

			std::string token, hdrVersion, hdrBuildStr;
			size_t len, pos;
			bool compatible = false;
			uint16_t hdrBuild, hdrBuildLow, hdrBuildHigh;

			for (uint32_t i = 0, j = 0; i < osTokens.size(); ++i, j = 0) {
				pos = (i == 0) ? 2 : 0;
				token = osTokens.at(i);
				len = token.length();
				hdrVersion = token.substr(pos, 2);

				if (deviceOsVersion != hdrVersion) {
					continue;
				}

				pos += 2;
				if (pos == len) {
					compatible = true;
					break;
				}

				hdrBuild = hdrBuildHigh = 0;
				hdrBuildLow = UINT16_MAX;

				while (pos < len) {
					hdrBuildStr = token.substr(pos, 4);
					std::swap(hdrBuildStr[0], hdrBuildStr[2]);
					std::swap(hdrBuildStr[1], hdrBuildStr[3]);
					std::istringstream ss(hdrBuildStr);
					ss >> std::hex >> hdrBuild;

					if (hdrBuildLow > hdrBuild) {
						hdrBuildLow = hdrBuild;
					}

					if (hdrBuildHigh < hdrBuild) {
						hdrBuildHigh = hdrBuild;
					}

					if (hdrBuildLow == hdrBuildHigh) { // single OS build
						if (moduleInfo.osBuild == hdrBuildLow) {
							compatible = true;
							i = osTokens.size();
							break;
						}
					} else if (hdrBuildLow < hdrBuildHigh) { // OS build range
						if (moduleInfo.osBuild >= hdrBuildLow && moduleInfo.osBuild <= hdrBuildHigh) {
							compatible = true;
							i = osTokens.size();
							break;
						}
					}
					pos += 4;
					j++;
				}
			}

			return compatible;
		}

		Error parseOsHeader(const std::string &line, const ModuleInfo &moduleInfo, std::string &errorMsg) {
			if (!std::regex_match(line, std::regex(OS_HEADER_PATTERN, std::regex_constants::icase))) {
				errorMsg = "Invalid OS header format (2). Header: " + line;
				return INVALID_FILE_CONTENT;
			}

			char aux[5];
			sprintf(aux, "%X%X", moduleInfo.osMajor, moduleInfo.osMinor);
			std::string deviceOsVersion(aux);
			sprintf(aux, "%04X", moduleInfo.osBuild);
			std::string deviceOsBuild(aux);

			std::string token, hdrVersion, hdrBuildStr;
			std::vector<std::string> osTokens = utils::split(line, ";");
			size_t len, pos;
			bool compatible = false;
			uint16_t hdrBuild, hdrBuildLow, hdrBuildHigh;

			for (uint32_t i = 0, j = 0; i < osTokens.size(); ++i, j = 0) {
				pos = (i == 0) ? 2 : 0;
				token = osTokens.at(i);
				len = token.length();
				hdrVersion = token.substr(pos, 2);

				if (deviceOsVersion != hdrVersion) {
					continue;
				}

				pos += 2;
				if (pos == len) {
					compatible = true;
					break;
				}

				hdrBuild = hdrBuildHigh = 0;
				hdrBuildLow = UINT16_MAX;

				while (pos < len) {
					hdrBuildStr = token.substr(pos, 4);
					std::swap(hdrBuildStr[0], hdrBuildStr[2]);
					std::swap(hdrBuildStr[1], hdrBuildStr[3]);
					std::istringstream ss(hdrBuildStr);
					ss >> std::hex >> hdrBuild;

					if (hdrBuildLow > hdrBuild) {
						hdrBuildLow = hdrBuild;
					}

					if (hdrBuildHigh < hdrBuild) {
						hdrBuildHigh = hdrBuild;
					}

					if (hdrBuildLow == hdrBuildHigh) { // single OS build
						if (moduleInfo.osBuild == hdrBuildLow) {
							compatible = true;
							i = osTokens.size();
							break;
						}
					} else if (hdrBuildLow < hdrBuildHigh) { // OS build range
						if (moduleInfo.osBuild >= hdrBuildLow && moduleInfo.osBuild <= hdrBuildHigh) {
							compatible = true;
							i = osTokens.size();
							break;
						}
					}
					pos += 4;
					j++;
				}
			}

			if (!compatible) {
				errorMsg = "Selected IQRF plugin is not compatible with device OS " + deviceOsVersion + "(" + deviceOsBuild + ")";
				return DEVICE_INCOMPATIBLE_FILE;
			}

			return NO_ERROR;
		}

		bool parseUpdateHeader(const std::string &line, uint8_t &updateTime) {
			std::smatch sm;
			if (!std::regex_match(line, sm, std::regex(UPDATE_TIME_HEADER_PATTERN))) {
				return false;
			}
			updateTime = (uint8_t)std::stoi(sm[1], nullptr, 10);
			return true;
		}

		bool validPluginHeaderDpa(const std::string &line) {
			return std::regex_match(line, std::regex(DPA_PLUGIN_HEADER_PATTERN));
		}

		bool validPluginHeaderOs(const std::string &line) {
			std::smatch sm;
			if (!std::regex_match(line, sm, std::regex(OS_PLUGIN_HEADER_PATTERN))) {
				return false;
			}
			if (sm.size() == 3 && sm[1].length() != 0 && sm[2].length() != 0) {
				uint8_t part = (uint8_t)std::stoi(sm[1], nullptr, 10);
				uint8_t whole = (uint8_t)std::stoi(sm[2], nullptr, 10);
				if (part > whole) {
					return false;
				}
			}
			return true;
		}

		bool isSeparator(const std::string &line) {
			return std::regex_match(line, std::regex(SEPARATOR_PATTERN));
		}

		void validateData(const std::string &line) {
			if (line.length() != LINE_LENGTH) {
				throw std::invalid_argument("IQRF plugin data line should be 40 characters long.");
			}
			if (!std::regex_match(line, std::regex(DATA_LINE_PATTERN, std::regex_constants::icase))) {
				throw std::invalid_argument("IQRF plugin data line contains non-hexadecimal characters.");
			}
		}

		bool validDataLine(const std::string &line, std::string &errorMsg) {
			if (line.length() != LINE_LENGTH) {
				errorMsg = "IQRF plugin data line should be 40 characters long.";
				return false;
			}
			if (!std::regex_match(line, std::regex(DATA_LINE_PATTERN, std::regex_constants::icase))) {
				errorMsg = "IQRF plugin data line contains non-hexadecimal characters.";
				return false;
			}
			return true;
		}
	}
}
