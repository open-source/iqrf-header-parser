/**
 * Copyright 2021-2025 IQRF Tech s.r.o.
 * Copyright 2021-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iqrf-header-parser/hex.h"

/// iqrf header parser namespace
namespace iqrf_header_parser {
	/// hex plugin namespace
	namespace hex {
		void validateRecord(const std::string &record) {
			size_t len = record.length();
			if (record[0] != ':') {
				throw std::invalid_argument("Intel HEX record should start with a colon character.");
			}
			if (len < RECORD_LENGTH_MIN) {
				throw std::invalid_argument("Intel HEX record length should be at least 11 characters long.");
			}
			if (len > RECORD_LENGTH_MAX) {
				throw std::invalid_argument("Intel HEX record length should be at most 521 characters long.");
			}
			if (len % 2 != 1) {
				throw std::invalid_argument("Intel HEX record should contain odd number of characters.");
			}
			if (!std::regex_match(record, std::regex(RECORD_PATTERN, std::regex_constants::icase))) {
				throw std::invalid_argument("Intel HEX record should contain only hexadecimal characters.");
			}
			if (!validRecordChecksum(record)) {
				throw std::invalid_argument("Incorrect Intel HEX record checksum.");
			}
			uint8_t recordType = utils::hexStringToByte(record, 7);
			if (recordType == 1 && record != END_OF_FILE) {
				throw std::invalid_argument("Invalid Intel HEX end-of-file record.");
			}
			if (recordType == 3 || recordType > 4) {
				throw std::invalid_argument("Unknown or unsupported Intel HEX record type.");
			}
			uint8_t byteCount = utils::hexStringToByte(record, 1);
			size_t detectedDataBytes = (len - RECORD_LENGTH_MIN) / 2;
			if (byteCount != detectedDataBytes) {
				throw std::invalid_argument("Byte count and detected data byte count mismatch: expected " + std::to_string(byteCount) + " detected " + std::to_string(detectedDataBytes));
			}
		}

		bool validRecordChecksum(const std::string &record) {
			size_t len = record.length() - 1;
			uint32_t sum = 0;
			std::string data = record.substr(1, len);
			for (uint32_t i = 0; i < len / 2; ++i) {
				sum += std::stoul(data.substr(i * 2, 2), nullptr, 16);
			}
			return (sum & 0xff) == 0;
		}

		void parseCompatibilityHeader(const std::string &record, uint8_t &os, uint8_t &mcu, uint8_t &tr) {
			if (!std::regex_match(record, std::regex(COMPATIBILITY_RECORD_HEADER_PATTERN, std::regex_constants::icase))) {
				throw std::invalid_argument("Invalid compatibility header.");
			}
			os = std::stoi(record.substr(9, 2), nullptr, 10);
			mcu = std::stoi(record.substr(13, 2), nullptr, 16);
			if (record.length() == 19) {
				tr = device::TrFamily::TR_7xD;
			} else {
				tr = std::stoi(record.substr(17, 2), nullptr, 16) - 0x80;
			}
		}

		void validateDeviceCompatibility(const std::string &record, const device::ModuleInfo &moduleInfo) {
			uint16_t byteCount = utils::hexStringToByte(record, 1);
			if (byteCount != 4 && byteCount != 6) {
				throw std::invalid_argument("Identification header record should have 4 or 6 data bytes.");
			}
			uint8_t recordOs = std::stoi(record.substr(9, 2), nullptr, 10);
			uint8_t deviceOs = moduleInfo.osMajor * 10 + moduleInfo.osMinor;
			if (recordOs != deviceOs) {
				throw std::invalid_argument("Selected HEX is for OS " + std::to_string(recordOs) + ", but the device OS is " + std::to_string(deviceOs));
			}
			uint8_t recordMcu = std::stoi(record.substr(13, 2), nullptr, 16);
			if (recordMcu != moduleInfo.mcuType) {
				throw std::invalid_argument("Selected HEX is for MCU " + std::to_string(recordMcu) + ", but the device MCU is " + std::to_string(moduleInfo.mcuType));
			}
			if (byteCount == 4) {
				device::TrFamily deviceTrFamily = device::getTrFamily(moduleInfo.mcuType, moduleInfo.trSeries);
				if (deviceTrFamily != device::TrFamily::TR_7xD) {
					throw std::invalid_argument("Selected HEX is for TR family " + std::to_string(device::TrFamily::TR_7xD) + ", but the device TR family is " + std::to_string(deviceTrFamily));
				} else {
					return;
				}
			}
			uint8_t recordTrFamily = std::stoi(record.substr(17, 2), nullptr, 16) - 0x80;
			if (!device::validMcuTrCombination(recordMcu, recordTrFamily)) {
				throw std::invalid_argument("Identification header has invalid combination of MCU (" + std::to_string(recordMcu) + ") and TR family (" + std::to_string(recordTrFamily) + ").");
			}
			device::TrFamily deviceTrFamily = device::getTrFamily(moduleInfo.mcuType, moduleInfo.trSeries);
			if (deviceTrFamily == device::TrFamily::UNKNOWN_FAMILY) {
				throw std::domain_error("Unable to identify device family.");
			}
			if (recordTrFamily != deviceTrFamily) {
				throw std::invalid_argument("Selected HEX is for TR family " + std::to_string(recordTrFamily) + ", but the device TR family is " + std::to_string(deviceTrFamily));
			}
		}
	}
}
