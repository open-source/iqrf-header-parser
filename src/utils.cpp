/**
 * Copyright 2021-2025 IQRF Tech s.r.o.
 * Copyright 2021-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "iqrf-header-parser/utils.h"

/// iqrf header parser namesapce
namespace iqrf_header_parser {
	/// utils namespace
	namespace utils {
		std::vector<std::string> split(const std::string &string, const std::string &delimiter) {
			std::string token;
			std::vector<std::string> tokens;
			if (!string.empty()) {
				size_t needle, start = 0, len = delimiter.length();
				while ((needle = string.find(delimiter, start)) != std::string::npos) {
					token = string.substr(start, needle - start);
					tokens.push_back(token);
					start = needle + len;
				}
				tokens.push_back(string.substr(start));
			}
			return tokens;
		}

		uint8_t charToUint8(const char c) {
			return (uint8_t)c - '0';
		}

		uint8_t hexStringToByte(const std::string &hex, const uint32_t &pos) {
			if (hex.length() == 0) {
				throw std::invalid_argument("Empty string.");
			}
			if (hex.length() < (pos + 2)) {
				throw std::range_error("Position is further than the length of the string.");
			}
			std::string byte = hex.substr(pos, 2);
			try {
				return (uint8_t)std::stoi(byte, nullptr, 16);
			} catch (const std::exception &e) {
				throw std::invalid_argument("Invalid hexadecimal string: " + hex);
			}
		}
	}
}
