/**
 * Copyright 2021-2025 IQRF Tech s.r.o.
 * Copyright 2021-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

/// iqrf header parser namespace
namespace iqrf_header_parser {
    /**
	 * Error code enum
	 */
	enum Error {
		NO_ERROR,
		UNSUPPORTED_UPLOAD_DEVICE = 4,
		INVALID_FILE_CONTENT,
		UNSUPPORTED_FILE_CONTENT,
		DEVICE_INCOMPATIBLE_FILE
	};
}
