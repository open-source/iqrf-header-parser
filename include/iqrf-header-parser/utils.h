/**
 * Copyright 2021-2025 IQRF Tech s.r.o.
 * Copyright 2021-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

#include <cstdint>
#include <regex>
#include <stdexcept>
#include <string>
#include <vector>

/// iqrf header parser namespace
namespace iqrf_header_parser {
	/// utils namespace
	namespace utils {
		/**
		 * Splits string by delimiter and returns vector of substrings
		 * @param string String to be split
		 * @param delimiter Characters to split by
		 * @return String split by delimiter in vector
		 */
		std::vector<std::string> split(const std::string &string, const std::string &delimiter);

		/**
		 * Converts ASCII character to 8bit unsigned integer
		 * @param c character
		 * @return uint8_t unsigned integer value
		 */
		uint8_t charToUint8(char c);

		/**
		 * Converts two hex string digits to a byte
		 * @param hex hex string
		 * @param pos position to read from
		 * @return uint8_t byte value
		 */
		uint8_t hexStringToByte(const std::string &hex, const uint32_t &pos);
	}
}
