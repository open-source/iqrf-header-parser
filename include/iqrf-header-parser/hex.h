/**
 * Copyright 2021-2025 IQRF Tech s.r.o.
 * Copyright 2021-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

#include <cstdint>
#include <cstddef>
#include <regex>
#include <stdexcept>
#include <string>

#include "device.h"
#include "utils.h"

/// iqrf header parser namespace
namespace iqrf_header_parser {
	/// hex plugin namespace
	namespace hex {
		/// intel hex record min length
		static const size_t RECORD_LENGTH_MIN = 11;
		/// intel hex record max length
		static const size_t RECORD_LENGTH_MAX = 521;
		/// intel hex record regex pattern
		static const std::string RECORD_PATTERN = ":[0-9a-f]+$";
		/// os, mcu and tr family header address
		static const uint16_t IDENTIFICATION_HEADER_ADDRESS = 0x7000;
		/// intel hex compatibility header regex pattern
		static const std::string COMPATIBILITY_RECORD_HEADER_PATTERN = "^:(0470{5}([0-9a-f]{2}00){2}|0670{5}([0-9a-f]{2}00){3})[0-9a-f]{2}$";
		/// eof record
		static const std::string END_OF_FILE = ":00000001FF";

		/**
		 * Validates hex record
		 * @param record Hex record
		 * @throws std::logic_error Hex record invalid reason
		 */
		void validateRecord(const std::string &record);

		/**
		 * Validates hex record checksum
		 * @param record Hex record
		 * @return true if checksum is correct, false otherwise
		 */
		bool validRecordChecksum(const std::string &record);

		/**
		 * Parses compatibility hex record
		 * @param record Compatibility hex record
		 * @param os OS storage variable
		 * @param mcu MCU storage variable
		 * @param tr TR storage variable
		 */
		void parseCompatibilityHeader(const std::string &record, uint8_t &os, uint8_t &mcu, uint8_t &tr);

		/**
		 * Checks is plugin is device compatible
		 * @param record Identification header hex record
		 * @param moduleInfo Device module information
		 */
		void validateDeviceCompatibility(const std::string &record, const device::ModuleInfo &moduleInfo);
	}
}

