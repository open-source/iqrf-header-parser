/**
 * Copyright 2021-2025 IQRF Tech s.r.o.
 * Copyright 2021-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

#include <cstdint>
#include <iomanip>
#include <regex>
#include <sstream>
#include <string>

#include "device.h"
#include "error.h"
#include "utils.h"

/// iqrf header parser namespace
namespace iqrf_header_parser {

	using namespace device;

	/// iqrf plugin namespace
    namespace iqrf {
		/// IQRF plugin data line length
		static const uint8_t LINE_LENGTH = 40;
		/// MCU header regex pattern
		static const std::string MCU_HEADER_PATTERN = "^#\\$\\w{2}$";
		/// OS header regex pattern
		static const std::string OS_HEADER_PATTERN = "^#\\$\\d{2}((\\;)(\\d{2})|([0-9a-f]{4}))*$";
		/// DPA plugin header regex pattern
		static const std::string DPA_PLUGIN_HEADER_PATTERN = "^#\\$Plug-in:\\sDPA-(Coordinator-(SPI|UART)|Node-(LP|STD(-UART|-SPI)?))-\\dx[DG]-V\\d{3}-\\d{6}$";
		/// OS plugin header regex pattern
		static const std::string OS_PLUGIN_HEADER_PATTERN = "^#\\$Plug-in:\\sChangeOS-TR\\dx([DG])?(?:-\\d{3}\\([0-9A-F]{4}\\)){2}(?:-([12])of([12]))?$";
		/// Update time header regex pattern
		static const std::string UPDATE_TIME_HEADER_PATTERN = "^#\\$UpdateTime:\\s(\\d+)$";
		/// Separator regex pattern
		static const std::string SEPARATOR_PATTERN = "^#\\s\\*+$";
		/// Data line regex pattern
		static const std::string DATA_LINE_PATTERN  = "^[a-f0-9]{40}$";

		/**
		 * Validates MCU header
		 * @param line MCU/TR header
		 */
		void validateMcuHeader(const std::string &line);

		/**
		 * Parses MCU header if valid
		 * @param line MCU/TR header
		 * @param mcu MCU type storage
		 * @param tr TR series storage
		 */
		void parseMcuHeader(const std::string &line, uint8_t &mcu, uint8_t &tr);

		/**
		 * Validates compatibility of selected IQRF plugin and target device
		 * @param line MCU/TR header
		 * @param moduleInfo Device module information
		 */
		void validateMcuCompatibility(const std::string &line, const ModuleInfo &moduleInfo);

		/**
		 * Validates and parses MCU header, stores error message in errorMsg
		 * @param line MCU and TR header
		 * @param mcu MCU type
		 * @param tr TR series
		 * @param errorMsg Error message container
		 * @return Error code
		 */
		Error parseMcuHeader(const std::string &line, const uint8_t &mcu, const uint8_t &tr, std::string &errorMsg);

		/**
		 * Validates OS header
		 * @param line OS header
		 */
		void validateOsHeader(const std::string &line);

		/**
		 * Parses OS header if valid
		 * @param line OS header
		 * @param os OS tokens storage
		 */
		void parseOsHeader(const std::string &line, std::vector<std::string> &os);

		/**
		 * Checks compatibility of selected IQRF plugin OS and target device
		 * @param osTokens OS tokens
		 * @param moduleInfo Device module information
		 * @return true Compatible
		 * @return false Not compatible
		 */
		bool osCompatible(const std::vector<std::string> &osTokens, const ModuleInfo &moduleInfo);

		/**
		 * Validates and parses OS header, stores error message in errorMsg
		 * @param line OS header
		 * @param moduleInfo Device module information
		 * @param errorMsg Error message container
		 * @return Error code
		 */
		Error parseOsHeader(const std::string &line, const device::ModuleInfo &moduleInfo, std::string &errorMsg);

		/**
		 * Validates and parses updateTime header, stores update time
		 *  in updateTime param if header is valid
		 * @param line Update time header
		 * @param updateTime Update time variable container
		 * @return true if header is valid, false otherwise
		 */
		bool parseUpdateHeader(const std::string &line, uint8_t &updateTime);

		/**
		 * Validates DPA plugin header
		 * @param line Plugin header
		 * @return true if header is valid, false otherwise
		 */
		bool validPluginHeaderDpa(const std::string &line);

		/**
		 * Validates IQRF OS plugin header
		 * @param line Plugin header
		 * @return true if header is valid, false otherwise
		 */
		bool validPluginHeaderOs(const std::string &line);

		/**
		 * Checks if comment line is a separator
		 * @param line Comment line
		 * @return true if line is a separator, false otherwise
		 */
		bool isSeparator(const std::string &line);

		/**
		 * Validates data record
		 * @param line Data record
		 */
		void validateData(const std::string &line);

		/**
		 * Validates data line length and content, stores error message in errorMsg
		 * @param line Data line
		 * @param errorMsg Error message container
		 * @return true if line is valid, false otherwise
		 */
		bool validDataLine(const std::string &line, std::string &errorMsg);
	}
}
