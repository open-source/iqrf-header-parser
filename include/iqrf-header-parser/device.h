/**
 * Copyright 2021-2025 IQRF Tech s.r.o.
 * Copyright 2021-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once

#include <cstdint>

/// iqrf header parser namespace
namespace iqrf_header_parser {
	/// device namespace
	namespace device {
		/**
		 * MCU type enum
		 */
		enum MCU {
			UNKNOWN_PIC,
			PIC16LF819,
			PIC16LF88,
			PIC16F886,
			PIC16LF1938,
			PIC16LF18877,
		};

		/**
		 * TR series enum
		 */
		enum TrFamily {
			UNKNOWN_FAMILY = -1,
			TR_5xD,
			TR_7xD,
			TR_7xG,
			TR_8xG,
		};

		/**
		 * TR series D enum
		 */
		enum TRD {
			TR_52D,
			TR_58D_RJ,
			TR_72D,
			TR_53D,
			TR_78D,
			TR_54D = 8,
			TR_55D,
			TR_56D,
			TR_76D,
			TR_77D,
			TR_75D
		};

		/**
		 * TR series G enum
		 */
		enum TRG {
			TR_82G,
			TR_72G = 2,
			TR_85G = 9,
			TR_86G,
			TR_76G,
			TR_75G = 13,
		};

		/**
		 * Device module information struct
		 */
		typedef struct {
			/// OS Major version
			uint8_t osMajor;
			/// OS Minor version
			uint8_t osMinor;
			/// OS Build number
			uint16_t osBuild;
			/// MCU type
			uint8_t mcuType;
			/// TR series
			uint8_t trSeries;
		} ModuleInfo;

		/**
		 * Validates MCU type value
		 * @param mcuType MCU type
		 * @return true valid
		 * @return false invalid
		 */
		bool validMcuType(const uint8_t &mcuType);

		/**
		 * Returns TR family based on MCU and TR
		 * @param mcu MCU type
		 * @param tr TR series
		 * @return TrFamily Module TR family
		 */
		TrFamily getTrFamily(const uint8_t &mcu, const uint8_t &tr);

		/**
		 * Validates TR series value
		 * @param trSeries TR series
		 * @return true valid
		 * @return false invalid
		 */
		bool validTrFamily(const uint8_t &trFamily);

		/**
		 * Validates 5xD TR series value
		 * @param tr TR series
		 * @return true valid
		 * @return false invalid
		 */
		bool validTr5xD(const uint8_t &tr);

		/**
		 * Validates TR 7xD series value
		 * @param tr TR series
		 * @return true valid
		 * @return false invalid
		 */
		bool validTr7xD(const uint8_t &tr);

		/**
		 * Validates TR 7xG series value
		 * @param tr TR series
		 * @return true valid
		 * @return false invalid
		 */
		bool validTr7xG(const uint8_t &tr);

		/**
		 * Validates TR 8xG series value
		 * @param tr TR series
		 * @return true valid
		 * @return false invalid
		 */
		bool validTr8xG(const uint8_t &tr);

		/**
		 * Validates MCU and TR combination in header
		 * @param mcu MCU type
		 * @param tr TR series
		 * @return true valid
		 * @return false invalid
		 */
		bool validMcuTrCombination(const uint8_t &mcu, const uint8_t &tr);
	}
}
