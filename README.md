# IQRF Header Parser

Header parser and validator for IQRF plugins

## Acknowledgement

This project has been made possible with a government grant by means of [the Ministry of Industry and Trade of the Czech Republic](https://www.mpo.cz/) in [the TRIO program](https://starfos.tacr.cz/cs/project/FV40132).
